#!/usr/bin/python

### WARNING ###
# This is a DIY product/solution so don’t use this for safety critical systems or in any situation where there could be a risk to life.
# There is no warranty, it may not work as expected or at all.
# The use of this project is done so entirely at your own risk. If in doubt, seek help.
# The use of this project may not be compliant with local laws or regulations - if in doubt, seek help.

import sys
import os
import subprocess
import struct
from pymodbus.client.sync import ModbusSerialClient as ModbusSClient
from pymodbus.exceptions import ModbusIOException as ModbusIOException
from pymodbus.compat import int2byte, byte2int
from pymodbus.pdu import ModbusRequest as ModbusRequest
from pymodbus.pdu import ModbusResponse as ModbusResponse
from pymodbus.pdu import ExceptionResponse as ExceptionResponse
#import logging

#FORMAT = ('%(message)s')
#logging.basicConfig(format=FORMAT)
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)

usbDevice = -1 # Put your USB device here if it's always the same

# Heartbeat not currently used as it seems like ME3000 doesn't care if you use it or not
class SofarHeartbeatResponse(ModbusResponse):
    function_code = 0x49 
    _rtu_byte_count_pos = 2

    def __init__(self, values=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self.values = values or []
    
    def encode(self):
        """ Encodes response pdu

        :returns: The encoded packet message
        """
        result = int2byte(len(self.values) * 2)
        for register in self.values:
            result += struct.pack('>H', register)
        return result

    def decode(self, data):
        """ Decodes response pdu

        :param data: The packet data to decode
        """
        byte_count = byte2int(data[0])
        self.values = []
        for i in range(1, byte_count + 1, 2):
            self.values.append(struct.unpack('>H', data[i:i + 2])[0])

class SofarHeartbeatRequest(ModbusRequest):

    function_code = 0x49
    _rtu_frame_size = 8

    def __init__(self, address, power=0, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self.address = address
        self.value = value 

    def encode(self):
        return struct.pack('>HH', self.address, self.value)

    def decode(self, data):
        self.address, self.value = struct.unpack('>HH', data)

    def execute(self, context):
        if self.value != 0x2202 or self.address != 0x2201:
            return self.doException(ModbusExceptions.IllegalValue)
        if not context.validate(self.function_code, self.address, self.value):
            return self.doException(ModbusExceptions.IllegalAddress)
        values = context.getValues(self.function_code, self.address, self.value)
        return SofarHeartbeatResponse(values)


# Standby will make the ME3000 do nothing (just waits)
class SofarStandbyResponse(ModbusResponse):
    function_code = 0x42 
    _rtu_byte_count_pos = 2

    def __init__(self, values=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self.values = values or []
    
    def encode(self):
        """ Encodes response pdu

        :returns: The encoded packet message
        """
        result = int2byte(len(self.values) * 2)
        for register in self.values:
            result += struct.pack('>H', register)
        return result

    def decode(self, data):
        """ Decodes response pdu

        :param data: The packet data to decode
        """
        byte_count = byte2int(data[0])
        self.values = []
        for i in range(1, byte_count + 1, 2):
            self.values.append(struct.unpack('>H', data[i:i + 2])[0])

class SofarStandbyRequest(ModbusRequest):

    function_code = 0x42
    _rtu_frame_size = 8

    def __init__(self, address, power=0, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self.address = address
        self.power = power 

    def encode(self):
        return struct.pack('>HH', self.address, self.power)

    def decode(self, data):
        self.address, self.power = struct.unpack('>HH', data)

    def execute(self, context):
        if self.power != 0x5555:
            return self.doException(ModbusExceptions.IllegalValue)
        if not context.validate(self.function_code, self.address, self.power):
            return self.doException(ModbusExceptions.IllegalAddress)
        values = context.getValues(self.function_code, self.address, self.power)
        return SofarStandbyResponse(values)

# Tell the ME3000 to charge/discharge/decide for itself
class SofarChargerResponse(ModbusResponse):
    function_code = 0x42 
    _rtu_byte_count_pos = 2

    def __init__(self, values=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self.values = values or []
    
    def encode(self):
        """ Encodes response pdu

        :returns: The encoded packet message
        """
        result = int2byte(len(self.values) * 2)
        for register in self.values:
            result += struct.pack('>H', register)
        return result

    def decode(self, data):
        """ Decodes response pdu

        :param data: The packet data to decode
        """
        byte_count = byte2int(data[0])
        self.values = []
        for i in range(1, byte_count + 1, 2):
            self.values.append(struct.unpack('>H', data[i:i + 2])[0])

class SofarChargerRequest(ModbusRequest):

    function_code = 0x42
    _rtu_frame_size = 8

    def __init__(self, address, power=0, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self.address = address
        self.power = power 

    def encode(self):
        return struct.pack('>HH', self.address, self.power)

    def decode(self, data):
        self.address, self.power = struct.unpack('>HH', data)

    def execute(self, context):
        if not (0 <= self.power <= 0xbb8):
            return self.doException(ModbusExceptions.IllegalValue)
        if not context.validate(self.function_code, self.address, self.power):
            return self.doException(ModbusExceptions.IllegalAddress)
        values = context.getValues(self.function_code, self.address, self.power)
        return SofarChargerResponse(values)


class Sofar(object):

   def findUSBDevice( self ):
     global usbDevice
     if usbDevice == -1:
       for d in range( 0, 9 ):
           if os.path.exists("/dev/ttyUSB" + str(d)):
              cmd = 'udevadm info -p  $(udevadm info -q path -n /dev/ttyUSB' + str(d) + ') | grep ID_MODEL_FROM_DATABASE'
              p = subprocess.Popen(cmd, shell=True, close_fds=True, stdout=subprocess.PIPE)
              (output,error) = p.communicate()
              if output.find( 'HL-340' ) != -1:
                  usbDevice = d
                  break
   
     if usbDevice == -1:
         print "I can't find the Sofar interface"
         sys.exit(3)
     else:
         print "I think Sofar is on /dev/ttyUSB%d" % usbDevice

   def signed( self, value ):
       if value > 32768:
           return value - 65535
       else:
           return value

   def __init__(self):
      self.findUSBDevice()

   def readSofar( self ):
      global usbDevice
      # Initialise values in case inverter is off
      sofar_dcv = 0
      sofar_dci = 0
      sofar_dcp = 0
      sofar_acp = 0
      sofar_acv = 0
      sofar_aci = 0
      sofar_acf = 0
      sofar_temp = 0
      battery_v = 0
      battery_i = 0
      battery_p = 0
      battery_level = 0
      battery_temp = 0
      battery_spare1 = 0
      battery_spare2 = 0
      feedin = 0
      rr = None

      namestr = 'Sofar'

      # Read the data from the sofar inverter
      print 'Reading from %s' % namestr
      try:
        inverter = ModbusSClient(method='rtu', port='/dev/ttyUSB' + str(usbDevice), baudrate=9600, stopbits=1, parity='N', bytesize=8, timeout=1)
        inverter.connect()
        # print 'Connected'
        rr = inverter.read_holding_registers(512,60,unit=1)
        # print 'Read registers'
      except:
        print 'Unable to read from %s' % namestr
      inverter.close()

      if rr is not None and type(rr) is not ModbusIOException and type(rr) is not ExceptionResponse:
        value=rr.registers[20]
        sofar_acp=(self.signed(value)*10) # convert to Watts
        value=rr.registers[12]
        sofar_acf=(float(value)/100) # convert to Hz
        value=rr.registers[6]
        sofar_acv=(float(value)/10) # convert to V
        value=rr.registers[7]
        sofar_aci=(float(value)/100) # convert to A
        value=rr.registers[14]
        battery_v=(float(value)/100) # convert to V
        value=rr.registers[13]
        battery_p=(self.signed(value)*10) # convert to Watts
        battery_i=(battery_p / battery_v) # in A
        value=rr.registers[16]
        battery_level=int(value)
        value=rr.registers[18]
        feedin=(self.signed(value)*10) # convert to Watts
        value=rr.registers[17]
        battery_temp=float(value) # in centigrade
        value=rr.registers[56]
        sofar_temp=float(value) # in centigrade
        value=rr.registers[24]
        sofar_today=(int(value)*10) # convert to WattHours
        value=rr.registers[28]
        sofar_total=(int(value)*65536) # in Wh (high word)
        value=rr.registers[29]
        sofar_total+=(int(value)) # in Wh (low word)

        print "AC Power: %d Watts" % sofar_acp
        print "AC Voltage: %f Volts" % sofar_acv
        print "AC Current: %f Amps" % sofar_aci
        print "AC Frequency: %f Hz" % sofar_acf
        print "ME3000 Temperature: %f Centigrade" % battery_temp
        print "ME3000 Today: %d Wh" % sofar_today
        print "ME3000 Total Ever: %d kWh" % sofar_total
        print "Battery Power: %d Watts" % battery_p
        print "Battery Voltage: %f Volts" % battery_v
        print "Battery Current: %f Amps" % battery_i
        print "Battery Level: %d%%" % battery_level
        print "Battery Temperature: %f Centigrade" % battery_temp
      else:
        print "Bad read, try again."
      return

   def batteryPower(self, level):
      global usbDevice
      namestr = 'Sofar'
      # Send command to the Sofar inverter
      print 'Connecting to %s' % namestr
      inverter = ModbusSClient(method='rtu', port='/dev/ttyUSB' + str(usbDevice), baudrate=9600, stopbits=1, parity='N', bytesize=8, timeout=1)
      inverter.connect()
      # print 'Connected'
      if level == 0x5555:
        inverter.register(SofarStandbyResponse)
        print 'Sending standby request'
        request = SofarStandbyRequest(0x100, level, unit=1)
      else:
        inverter.register(SofarChargerResponse)
        if level == 0x6666:
          print 'Sending charger auto request'
          request = SofarChargerRequest(0x103, 0, unit=1)
        else:
          # The charger has levels from 0 to 3000 watts
          level = int(level) # Just in case ;)
          if level > 3000:
            level = 3000
          elif level < -3000:
            level = -3000
          self.requestedRate = level
          if level > 0:  # (charge)
            print 'Sending charge %d request' % level
            request = SofarChargerRequest(0x102, level, unit=1)
          else: #if level <= 0:  # (discharge)
            print 'Sending discharge %d request' % (level * -1)
            request = SofarChargerRequest(0x101, level * -1, unit=1)
      try:
        result = inverter.execute(request)
      except ModbusExceptions as e:
        print 'Unable to command %s' % namestr
      print 'Response was: %X' % (result.values[0])
      inverter.close()


if __name__ == "__main__":

    if len(sys.argv) >= 2:
        me3000 = Sofar()
        if 'query' == sys.argv[1]:
          me3000.readSofar()
        elif 'auto' == sys.argv[1]:
          me3000.batteryPower(0x6666) # Special code for auto
        elif 'standby' == sys.argv[1]:
          me3000.batteryPower(0x5555) # Special code for standby
        elif 'charge' == sys.argv[1]:
          rate = int(sys.argv[2])
          me3000.batteryPower(rate)
        elif 'discharge' == sys.argv[1]:
          rate = int(sys.argv[2])
          me3000.batteryPower(rate)
        else:
            print "Unknown command %s" % sys.argv[1]
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s query|auto|standby|charge [rate]|discharge [rate]" % sys.argv[0]
        print "rate: in Watts (integer between 0 and 3000)"
        sys.exit(2)
