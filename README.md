# Control script for the Sofar ME3000SP #

### WARNING ###

This is a DIY product/solution so don’t use this for safety critical systems or in any situation where there could be a risk to life.

There is no warranty, it may not work as expected or at all.

The use of this project is done so entirely at your own risk. If in doubt, seek help.

The use of this project may not be compliant with local laws or regulations - if in doubt, seek help.

### How to use this script? ###

* Download the script.
* Make sure you have Python 2.x.
* Put your inverter into passive mode (this is done from the menus on the unit itself).

The script will attempt to find your USB port/device but if you know it already, you can edit the script and change this line from -1 to the ttyUSB number of your device.
```
usbDevice = -1 # Put your USB device here if it's always the same
```

So if your device is on /dev/ttyUSB3 then you would change the above line from -1 to 3, like this:
```
usbDevice = 3 # Put your USB device here if it's always the same
```

### What does this script do? ###

* Charge any value between 1 and 3000 Watts.
* Discharge any value between 1 and 3000 Watts.
* Automatically charge/discharge based on CT clamp (normal operation).
* Put the ME3000 into standby mode (it does nothing).

### What could it be used for? ###

* If you wanted to see what the inverter is doing, or store the information for drawing nice graphs etc.
* Set up a cron job to charge the battery at night when electic is cheaper (if you are on a tariff such as Octopus Go) so that you can use that during the day when electic is more expensive.
* Potentially you could also use it to export stored power to the grid when demand is high to earn money (though I am not currently aware of any such schemes).
### Octopus Go ###

If you have one of these devices or an electric car you should seriously consider moving to a time-of-day tariff to make the most of cheap rate electricity available at night time. Octopus Energy has a tariff called Go that lets you have electrictiy for just 5p per kWh between the hours of 00:30 and 04:30.

If you sign up to Octopus Go, please consider using my link <https://share.octopus.energy/sky-lane-902> and we will both get a £50 credit.

### Examples ###

You may or may not need the sudo depending on your device permissions (it does no harm to use it).

Query the inverter:
```
sudo ./sofar.py query
```

Tell inverter to charge at 1000 Watts:
```
sudo ./sofar.py charge 1000
```

Tell inverter to discharge at 500 Watts:
```
sudo ./sofar.py discharge 500
```

Tell inverter to match load (must have CT clamps installed):
```
sudo ./sofar.py auto
```
